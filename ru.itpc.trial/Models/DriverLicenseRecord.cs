﻿using System;

namespace ru.itpc.trial.Models
{
	public class DriverLicenseRecord : DriverLicense, Identified<int>
	{
		int id;
		public string Class { get; set; }
		public DateTime Issued { get; set; }
		public DateTime Expires { get; set; }
		public string OwnerFirstName { get; set; }
		public string OwnerLastName { get; set; }
		public DateTime OwnerBirthDate { get; set; }
		public int Id { get { return id; } }

		public DriverLicenseRecord()
		{}

		public DriverLicenseRecord(int id, string licenseClass, DateTime expiration, PersonRecord person)
		{
			OwnerFirstName = person.FirstName;
			OwnerLastName = person.LastName;
			OwnerBirthDate = person.BirthDate;
			Class = licenseClass;
			Expires = expiration;
			this.id = id;
			Issued = DateTime.Now;
		}
	}
}