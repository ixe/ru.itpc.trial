﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ru.itpc.trial.Models 
{
	public class PersonEnumerable<T> : IEnumerable<T>, ICollection<T>
	{
		private List<T> items;

		public PersonEnumerable()
		{
			items = new List<T>();
		}

		public IEnumerator<T> GetEnumerator()
		{
			return items.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return items.GetEnumerator ();
		}


		public void Add(T item)
		{
			items.Add (item);
		}
		public void Clear()
		{
			items.Clear ();
		}
		public bool Contains(T item)
		{
			return items.Contains (item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			items.CopyTo(array, arrayIndex);
		}
		public int Count
		{
			get 
			{
				return this.Count;
			}
		}
		public bool IsReadOnly
		{
			get 
			{
				return false;
			}
		}
		public bool Remove(T item)
		{
			return items.Remove (item);
		}

	}
}