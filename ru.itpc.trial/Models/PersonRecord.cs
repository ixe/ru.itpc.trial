﻿using System;

namespace ru.itpc.trial.Models
{
	public class PersonRecord: Person, Identified<string>
	{
		string id;
		string firstName;
		string lastName;
		DateTime birthDate;

		public string Id { get { return id;	} }
		public string FirstName { get { return firstName; }  }
		public string LastName { get { return lastName; } }
		public DateTime BirthDate { get { return birthDate; } }

		public PersonRecord()
		{}

		public PersonRecord(string fName, string lName)
		{
			firstName = fName;
			lastName = lName;
			birthDate = DateTime.Now;
			id = Guid.NewGuid().ToString();
		}

		public PersonRecord(string fName, string lName, DateTime bDate)
		{
			firstName = fName;
			lastName = lName;
			birthDate = bDate;
			id = Guid.NewGuid().ToString();
		}
	}
}