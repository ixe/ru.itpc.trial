﻿using System;

namespace ru.itpc.trial.Data
{
	public class DefaultDataContext : StorageDataContext  
	{  
		static DefaultDataContext instance = null;  

		DefaultDataContext()  
		{  
		}  

		public static DefaultDataContext Instance  
		{  
			get  
			{  
				if (instance == null)  
				{  
					instance = new DefaultDataContext();  
				}  
				return instance;  
			}   
		}  
	}  
}
